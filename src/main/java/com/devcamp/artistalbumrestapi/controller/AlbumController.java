package com.devcamp.artistalbumrestapi.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.artistalbumrestapi.Service.AlbumService;
import com.devcamp.artistalbumrestapi.model.Album;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AlbumController {
    @Autowired
    private AlbumService albumService;

    @GetMapping("/album-info")
    public Album getAlbumInfo(@RequestParam(name= "id",required = true)int id){
        Album findAlbum = albumService.filterAlbum(id);
        
        return findAlbum;
    }
}
