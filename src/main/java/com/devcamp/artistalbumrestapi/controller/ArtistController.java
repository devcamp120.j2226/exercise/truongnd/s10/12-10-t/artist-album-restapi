package com.devcamp.artistalbumrestapi.controller;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.artistalbumrestapi.Service.ArtistService;
import com.devcamp.artistalbumrestapi.model.Artist;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ArtistController {
    @Autowired
    private ArtistService artistService;

    @GetMapping("/artists")
    public ArrayList<Artist> getAllArtist(){
        ArrayList<Artist> allArtist = artistService.getAllArtist();

        return allArtist;
    }

    @GetMapping("/artist-info")
    public Artist getCountryInfo(@RequestParam(name = "id",required = true)int id){
        ArrayList<Artist> allArtist = artistService.getAllArtist();

        Artist findArtist = new Artist();

        for (Artist artistElement : allArtist) {
            if(artistElement.getId() == id){
                findArtist = artistElement;
            }
        }

        return findArtist;
    }
    @GetMapping("/artists/{index}")
    public Artist filterIndexParam(@PathVariable(value = "index") int index){
        return artistService.filterIndex(index);
    } 
}
