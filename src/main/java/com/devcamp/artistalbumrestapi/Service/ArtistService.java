package com.devcamp.artistalbumrestapi.Service;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.artistalbumrestapi.model.Artist;

@Service
public class ArtistService {
    @Autowired
    private AlbumService albumService;

    Artist blackpink = new Artist(1,"Black Pink");
    Artist bigbang = new Artist(2,"Big Bang");
    Artist twice = new Artist(3,"Twice");

    ArrayList<Artist> allArtistMain = new ArrayList<>();

    public ArrayList<Artist> getAllArtist(){
        ArrayList<Artist> allArtist = new ArrayList<>();

        blackpink.setAlbums(albumService.getAlbumBlackPink());
        bigbang.setAlbums(albumService.getAlbumBigBang());
        twice.setAlbums(albumService.getAlbumTwice());

        allArtist.add(blackpink);
        allArtist.add(bigbang);
        allArtist.add(twice);
        allArtistMain.addAll(allArtist);
         
        return allArtist;
    }
    public Artist filterIndex(int indexParam){
        Artist result = new Artist();
        if(indexParam >= 0 && indexParam <= 6){
            result = allArtistMain.get(indexParam);
        }
        return result;
    }
}
