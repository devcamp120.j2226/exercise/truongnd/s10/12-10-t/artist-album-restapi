package com.devcamp.artistalbumrestapi.Service;

import org.springframework.stereotype.Service;
import java.util.*;
import com.devcamp.artistalbumrestapi.model.Album;

@Service
public class AlbumService {
    Album bornpink = new Album(101,"Born Pink",songsBornPink());
    Album squareup = new Album(102,"Square Up",songsSquareUp());
    Album thenow = new Album(103,"The Now",songsTheNow());

    Album remember = new Album(201,"Remember",songsRemember());
    Album made = new Album(202,"Made",songsMade());
    Album number1 = new Album(203,"Number One",songsNumberOne());

    Album celebrate = new Album(301,"Celebrate",songsCelebrate());
    Album perfectworld = new Album(302,"Perfect World",songsPerfecWorld());
    Album bdz = new Album(303,"BDZ",songsBDZ());

    public ArrayList<String> songsCelebrate(){
        ArrayList<String> list = new ArrayList<String>();
        list.add("Celebrate");
        list.add("Tick Tock");
        list.add("That's all I'm saying");
        list.add("Bitter Sweet");

        return list;
    }
    public ArrayList<String> songsPerfecWorld(){
        ArrayList<String> list = new ArrayList<String>();
        list.add("Perfect Love");
        list.add("Good At Love");
        list.add("Kura Kura");
        list.add("In The Summer");

        return list;
    }
    public ArrayList<String> songsBDZ(){
        ArrayList<String> list = new ArrayList<String>();
        list.add("BDZ");
        list.add("One More Time");
        list.add("Candy Pop");
        list.add("Say It Again");

        return list;
    }
    public ArrayList<String> songsBornPink(){
        ArrayList<String> list = new ArrayList<String>();
        list.add("Pink Venom");
        list.add("Typa Girl");
        list.add("Hard To Love");
        list.add("Tally");

        return list;
    }
    public ArrayList<String> songsSquareUp(){
        ArrayList<String> list = new ArrayList<String>();
        list.add("Ddu Ddu Ddu Ddu");
        list.add("Realy");
        list.add("Forever Young");
        list.add("See You Later");

        return list;
    }
    public ArrayList<String> songsTheNow(){
        ArrayList<String> list = new ArrayList<String>();
        list.add("Kill This Love");
        list.add("How You Like That");
        list.add("Playing The Fire");
        list.add("As if your last");

        return list;
    }
    public ArrayList<String> songsRemember(){
        ArrayList<String> list = new ArrayList<String>();
        list.add("Oh, Ah, Oh");
        list.add("Wonderful");
        list.add("Strong Baby");
        list.add("Day By Day");

        return list;
    }
    public ArrayList<String> songsMade(){
        ArrayList<String> list = new ArrayList<String>();
        list.add("Girl Friend");
        list.add("Loser");
        list.add("Bang Bang Bang");
        list.add("Last Dance");

        return list;
    }
    public ArrayList<String> songsNumberOne(){
        ArrayList<String> list = new ArrayList<String>();
        list.add("Intro");
        list.add("Make Love");
        list.add("Haru Haru");
        list.add("How Gee");

        return list;
    }
    
    public ArrayList<Album> getAlbumBlackPink(){
        ArrayList<Album> albumBlackPink = new ArrayList<>();

        albumBlackPink.add(bornpink);
        albumBlackPink.add(squareup);
        albumBlackPink.add(thenow);

        return albumBlackPink;
    }
    public ArrayList<Album> getAlbumBigBang(){
        ArrayList<Album> albumBigBang = new ArrayList<>();

        albumBigBang.add(remember);
        albumBigBang.add(made);
        albumBigBang.add(number1);

        return albumBigBang;
    }
    public ArrayList<Album> getAlbumTwice(){
        ArrayList<Album> albumTwice = new ArrayList<>();

        albumTwice.add(celebrate);
        albumTwice.add(perfectworld);
        albumTwice.add(bdz);

        return albumTwice;
    }
    public Album filterAlbum(int id){
        ArrayList<Album> allAlbum = new ArrayList<>();

        allAlbum.add(bornpink);
        allAlbum.add(squareup);
        allAlbum.add(thenow);
        allAlbum.add(remember);
        allAlbum.add(made);
        allAlbum.add(number1);
        allAlbum.add(celebrate);
        allAlbum.add(bdz);
        allAlbum.add(perfectworld);
         
        Album findAlbum = new Album();

        for (Album albumElement : allAlbum) {
            if(albumElement.getId() == id){
                findAlbum = albumElement;
            }
        }

        return findAlbum;
    }
   
}
