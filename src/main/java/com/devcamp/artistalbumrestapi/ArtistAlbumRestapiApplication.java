package com.devcamp.artistalbumrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArtistAlbumRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArtistAlbumRestapiApplication.class, args);
	}

}
